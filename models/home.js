let db = require('../configDb');

module.exports.getDerniereMajGrandprix = function (callback) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="SELECT gpnum, gpnom, gpdate, gpdatemaj FROM grandprix g ORDER BY gpdatemaj DESC LIMIT 1";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};
