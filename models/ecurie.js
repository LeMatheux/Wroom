/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/

let db = require('../configDb');

/*
* Récupérer l'intégralité les écuries avec l'adresse de la photo du pays de l'écurie
* @return Un tableau qui contient le N°, le nom de l'écurie et le nom de la photo du drapeau du pays
*/
module.exports.getListeEcurie = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						let sql ="SELECT ecunum, payadrdrap, ecunom FROM ecurie e INNER JOIN pays p ";
						sql= sql + "ON p.paynum=e.paynum ORDER BY ecunom";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};

module.exports.getDetailsEcurie = function (callback, ecunum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT ecunom, ecunomdir, ecuadrsiege, ecuadresseimage, fpnom, paynom FROM ecurie e JOIN pays p ON e.paynum = p.paynum JOIN fourn_pneu fp ON fp.fpnum = e.fpnum WHERE ecunum = " + ecunum + ";";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getEcurieSponsors = function (callback, ecunum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT sponom, sposectactivite FROM sponsor s JOIN finance f ON s.sponum = f.sponum WHERE ecunum = " + ecunum + ";";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getPilotesEcurie = function (callback, ecunum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT p.pilnum, pilnom, pilprenom, phoadresse FROM pilote p JOIN photo ph ON p.pilnum = ph.pilnum WHERE ecunum = " + ecunum + " AND phonum = 1;";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getVoituresEcurie = function (callback, ecunum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT typelibelle, voinom, voiadresseimage FROM voiture v JOIN type_voiture tv ON v.typnum = tv.typnum WHERE ecunum = " + ecunum + ";";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};
