let db = require('../configDb');

module.exports.getPremiereLettrePilotes = function (callback) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT DISTINCT SUBSTRING(pilnom, 1, 1) as init FROM pilote ORDER BY init ASC;";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getPilotesInit = function(callback, init) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            var sql = "SELECT DISTINCT pi.pilnum AS numero, phoadresse, pilnom, pilprenom FROM pilote pi JOIN photo ph ON pi.pilnum = ph.pilnum WHERE pilnom LIKE '" + init + "%' AND phonum = 1 ORDER BY pilnom;";
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};

module.exports.getDetailsPilote = function (callback, pilnum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT pilnom, pilprenom, pildatenais, paynat, pilpoids, piltaille, piltexte, ecunom FROM pilote pi JOIN pays pa ON pi.paynum = pa.paynum LEFT JOIN ecurie e ON pi.ecunum = e.ecunum WHERE pilnum = " + pilnum + ";";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getSponsorsPilote = function (callback, pilnum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT sponom, sposectactivite FROM sponsor s JOIN sponsorise e ON s.sponum = e.sponum WHERE pilnum = " + pilnum + ";";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getPhotosPilote = function (callback, pilnum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT phosujet, phocommentaire, phoadresse FROM photo WHERE pilnum = " + pilnum + " AND phonum != 1;";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getPhotoPrincipalePilote = function (callback, pilnum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT phoadresse FROM photo WHERE pilnum = " + pilnum + " AND phonum = 1;";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};
