let db = require('../configDb');

module.exports.getListeGP = function (callback) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="SELECT gpnum, payadrdrap, gpnom FROM grandprix gp JOIN circuit c ON gp.cirnum = c.cirnum JOIN pays p ON p.paynum = c.paynum ORDER BY gpnom;";
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getDetailsGP = function (callback, gpnum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="SELECT gpnom, gpdate, gpcommentaire FROM grandprix gp WHERE gpnum = " + gpnum + ";";
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getPilotesResultats = function (callback, gpnum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql ="SELECT pilnom, pilprenom, pilpoints, tempscourse FROM course c JOIN pilote p ON c.pilnum = p.pilnum WHERE gpnum = " + gpnum + " AND pilpoints IS NOT NULL ORDER BY pilpoints DESC;";
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};
