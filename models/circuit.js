let db = require('../configDb');

module.exports.getListeCircuits = function (callback) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT cirnum, cirnom, payadrdrap FROM circuit c JOIN pays p ON c.paynum = p.paynum ORDER BY cirnom ASC;";
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};

module.exports.getDetailsCircuit = function (callback, cirnum) {
	db.getConnection(function(err, connexion){
        if(!err){
						let sql = "SELECT cirnom, cirlongueur, cirnbspectateurs, ciradresseimage, cirtext, paynom FROM circuit c JOIN pays p ON c.paynum = p.paynum WHERE cirnum = " + cirnum;
						//console.log (sql);
            connexion.query(sql, callback);

            connexion.release();
         }
      });
};
