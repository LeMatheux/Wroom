module.exports.AdminIndex = function(request, response){
    console.log(request.session.accesAdmin);
    if (request.session.accesAdmin != 1)
      response.redirect('/');
    else {
      response.title = "Administration de WROOM (IUT du Limousin)";
      console.log("Acces admin");
      response.render('accueil_admin', response);
    }
}
