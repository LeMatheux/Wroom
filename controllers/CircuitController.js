let model = require('../models/circuit.js');
let async = require('async')

// ////////////////////// L I S T E R     C I R C U I T S

module.exports.ListerCircuit = function(request, response){
    response.title = 'Liste des circuits';
    model.getListeCircuits( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeCircuits = result;
    response.render('listerCircuit', response);
  });
}

module.exports.DetailsCircuit = function(request, response){
    let cirnum = request.params.cirnum;
    response.title = 'Liste des circuits - Détails';
    async.parallel ([
        function (callback) {
            model.getListeCircuits(function(err, result) { callback(null, result) });
        } ,
        function (callback) {
            model.getDetailsCircuit(function(errE, resultE) { callback(null, resultE) }, cirnum);
        } ,
        ],
        function (err, result) {
            if (err) {
              console.log(err);
              return;
            }
            response.listeCircuits = result[0];
            response.details = result[1][0];
            response.render('listerCircuit', response);
        }
    );
}
