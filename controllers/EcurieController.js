let model = require('../models/ecurie.js');
let async = require('async');

   // //////////////////////// L I S T E R  E C U R I E S

module.exports.ListerEcurie = function(request, response){
   response.title = 'Liste des écuries';
    model.getListeEcurie( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
        response.listeEcuries = result;
        response.render('listerEcurie', response);
    });
}

module.exports.DetailsEcurie = function(request, response){
    let ecunum = request.params.ecunum;
    response.title = 'Liste des écuries - Détails';
    async.parallel ([
        function (callback) {
            model.getListeEcurie(function(err, result) { callback(null, result) });
        } ,
        function (callback) {
            model.getDetailsEcurie(function(errE, resultE) { callback(null, resultE) }, ecunum);
        } ,
        function (callback) {
            model.getEcurieSponsors(function(errE, resultE) { callback(null, resultE) }, ecunum);
        } ,
        function (callback) {
            model.getPilotesEcurie(function(errE, resultE) { callback(null, resultE) }, ecunum);
        } ,
        function (callback) {
            model.getVoituresEcurie(function(errE, resultE) { callback(null, resultE) }, ecunum);
        } ,
        ],
        function (err, result) {
            if (err) {
              console.log(err);
              return;
            }
            response.listeEcuries = result[0];
            response.details = result[1][0];
            response.sponsors = result[2];
            response.pilotes = result[3];
            response.voitures = result[4];
            response.render('listerEcurie', response);
        }
    );
}
