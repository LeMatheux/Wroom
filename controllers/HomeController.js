let model = require('../models/home.js');

  // ////////////////////////////////////////////// A C C U E I L
module.exports.Index = function(request, response){
    response.title = "Bienvenue sur le site de WROOM (IUT du Limousin).";
    model.getDerniereMajGrandprix( function (err, result) {
        if (err) {
            // gestion de l'erreur
            console.log(err);
            return;
        }
    response.majGP = result[0];
    response.render('home', response);
});
}

module.exports.NotFound = function(request, response) {
    response.title = "Page non trouvée";
    response.render('notFound', response);
}
