let model = require('../models/pilote.js');
let async = require('async')

// ///////////////////////// R E P E R T O I R E    D E S    P I L O T E S

module.exports.Repertoire = 	function(request, response){
   response.title = 'Répertoire des pilotes';
   model.getPremiereLettrePilotes( function (err, result) {
       if (err) {
           // gestion de l'erreur
           console.log(err);
           return;
       }
       response.listeInitiales = result;
      response.render('repertoirePilotes', response);
  });
}

module.exports.RepertoireInit = 	function(request, response){
   let init = request.params.init;
   response.title = 'Répertoire des pilotes - ' + init;
   async.parallel ([
       function (callback) {
           model.getPremiereLettrePilotes(function(err, result) { callback(null, result) });
       } ,
       function (callback) {
           model.getPilotesInit(function(errE, resultE) { callback(null, resultE) }, init);
       } ,
       ],
       function (err, result) {
           if (err) {
             console.log(err);
             return;
           }
           response.listeInitiales = result[0];
           response.pilotes = result[1];
           response.render('repertoirePilotes', response);
       }
   );
}

module.exports.RepertoirePilote = function(request, response){
   response.title = 'Répertoire des pilotes - Détails';
   let pilnum = request.params.pilnum;
   async.parallel ([
       function (callback) {
           model.getPremiereLettrePilotes(function(err, result) { callback(null, result) });
       } ,
       function (callback) {
           model.getDetailsPilote(function(errE, resultE) { callback(null, resultE) }, pilnum);
       } ,
       function (callback) {
           model.getSponsorsPilote(function(errE, resultE) { callback(null, resultE) }, pilnum);
       } ,
       function (callback) {
           model.getPhotosPilote(function(errE, resultE) { callback(null, resultE) }, pilnum);
       } ,
       function (callback) {
           model.getPhotoPrincipalePilote(function(errE, resultE) { callback(null, resultE) }, pilnum);
       } ,
       ],
       function (err, result) {
           if (err) {
             console.log(err);
             return;
           }
           response.listeInitiales = result[0];
           response.details = result[1][0];
           if (result[2].length > 0)
              response.sponsors = result[2];
           if (result[3].length > 0)
              response.photos = result[3];
           response.photoPrincipale = result[4][0];
           response.render('repertoirePilotes', response);
       }
   );
}
