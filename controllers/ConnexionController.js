let model = require('../models/connexion.js');
let crypto = require('crypto-js');

module.exports.PageConnexion = function(request, response){
	   response.title = 'Page de connexion';

     response.erreur = 0;
		 response.render('connexion', response);
};

module.exports.VerificationConnexion = function(request, response){
	response.title = 'Page de connexion';
  let login = request.body.login;
  let mdp = crypto.SHA1(request.body.mdp);

	model.getResultatConnexion( function (err, result) {
		 if (err) {
				 // gestion de l'erreur
				 console.log(err);
				 return;
		 }
		 response.connecte = result[0]['bonMdp'];
		 console.log(response.connecte);
     if (response.connecte == 1) {
			 	request.session.accesAdmin = 1;
		    response.redirect('/admin');
     } else {
        response.erreur = 1;
		    response.render('connexion', response);
     }
	}, login, mdp);
};
