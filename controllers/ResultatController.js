let model = require('../models/resultats.js');
let async = require('async');

  // //////////////////////////L I S T E R    R E S U L T A T S
module.exports.ListerResultat = function(request, response){
	response.title = 'Liste des résulats des grands prix';

	model.getListeGP( function (err, result) {
		 if (err) {
				 // gestion de l'erreur
				 console.log(err);
				 return;
		 }
		 response.listeGP = result;
		 response.render('listerResultat', response);
	});
};

module.exports.DetailsResultat = function(request, response){
  let gpnum = request.params.gpnum;
	response.title = 'Liste des résulats des grands prix - Détails';

	async.parallel ([
			function (callback) {
					model.getListeGP(function(err, result) { callback(null, result) });
			} ,
			function (callback) {
					model.getDetailsGP(function(errE, resultE) { callback(null, resultE) }, gpnum);
			} ,
			function (callback) {
					model.getPilotesResultats(function(errE, resultE) { callback(null, resultE) }, gpnum);
			} ,
			],
			function (err, result) {
					if (err) {
						console.log(err);
						return;
					}
					response.listeGP = result[0];
					response.details = result[1][0];
					response.resultats = result[2];
					response.render('listerResultat', response);
			}
	);
};
