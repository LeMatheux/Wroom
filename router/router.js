
let HomeController = require('./../controllers/HomeController');
let ResultatController = require('./../controllers/ResultatController');
let EcurieController = require('./../controllers/EcurieController');
let PiloteController = require('./../controllers/PiloteController');
let CircuitController = require('./../controllers/CircuitController');
let ConnexionController = require('./../controllers/ConnexionController');
let AdminAccueil = require('./../admin/controllers/AdminAccueil');

// Routes
module.exports = function({app:app, app_admin:app_admin}){

// Main Routes
    app.get('/', HomeController.Index);
    app.get('/accueil', HomeController.Index);

// pilotes
    app.get('/repertoirePilotes', PiloteController.Repertoire);
    app.get('/repertoirePilotes/:init', PiloteController.RepertoireInit);
    app.get('/repertoirePilotes/detailsPilote/:pilnum', PiloteController.RepertoirePilote);

 // circuits
   app.get('/circuits', CircuitController.ListerCircuit);
   app.get('/circuits/detailsCircuit/:cirnum', CircuitController.DetailsCircuit);

// Ecuries
   app.get('/ecuries', EcurieController.ListerEcurie);
   app.get('/ecuries/detailsEcurie/:ecunum', EcurieController.DetailsEcurie);

 //Résultats
   app.get('/resultats', ResultatController.ListerResultat);
   app.get('/resultats/detailsGP/:gpnum', ResultatController.DetailsResultat);

//Connexion
   app.get('/connexion', ConnexionController.PageConnexion);
   app.post('/connexion', ConnexionController.VerificationConnexion);

//Accueil admin
   app_admin.get('/admin', AdminAccueil.AdminIndex);
   app_admin.post('/admin', AdminAccueil.AdminIndex);

// tout le reste
   app.get('*', HomeController.NotFound);
   app.post('*', HomeController.NotFound);

};
